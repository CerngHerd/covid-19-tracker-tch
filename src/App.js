import React, {useEffect, useState} from 'react';
import InfoBox from './InfoBox';
import Map from './Map';
import Table from './Table';
import { sortData, prettyPrintStat } from './util';
import LineGraph from './LineGraph';
import {MenuItem, FormControl, Select, Card, CardContent} from "@material-ui/core";
import "leaflet/dist/leaflet.css";
import './App.css';

function App() {

  const [ countries, setCountries ] = useState([]);
  const [ country, setCountry ] = useState('worldwide');
  const [ countryInfo, setCountryInfo ] = useState({});
  const [ tableData, setTableData ] = useState([]);
  const [ mapCenter, setMapCenter] = useState({ lat: 34.80746, lng: -40.4796 });
  const [ mapZoom, setMapZoom ] = useState(3);
  const [ mapCountries, setMapCountries ] = useState([]);
  const [ casesType, setCasesType ] = useState("cases");

  useEffect(() => {
    fetch("https://disease.sh/v3/covid-19/all")
    .then(response => response.json())
    .then(data=>{
      setCountryInfo(data);
    });
  }, [])

  useEffect(() => {
    // useEffect = run a piece of code 
    // based on a given condition
    // the code inside here will run once
    // when the conponent loads and not again
    // async -> send a request to server, wait for it, and do something with info

    const getCountriesData = async () => {
      await fetch ("https://disease.sh/v3/covid-19/countries")
      .then(response=>response.json())
      .then((data) => {
        const countries = data.map((country) => (
          {
            name: country.country, //United Stated, United Kingdom
            value: country.countryInfo.iso2 //UK, USA, ...
          }));

          const sortedData = sortData(data);
          setTableData(sortedData);
          setMapCountries(data);
          setCountries(countries);
      });
    };

    getCountriesData();
  }, []);

  const onCountryChange = async (event) => {
    const countryCode = event.target.value;
    

    const url = 
      countryCode === 'worldwide' 
      ? 'https://disease.sh/v3/covid-19/all' 
      : `https://disease.sh/v3/covid-19/countries/${countryCode}`;

    await fetch(url)
    .then(response => response.json())
    .then(data => {
      setCountry(countryCode);
      // All of the data ...
      // All of the data from country
      setCountryInfo(data);
      setMapCenter([data.countryInfo.lat, data.countryInfo.long]);
      setMapZoom(4);
    });
  };

  console.log('country info >>> ', countryInfo)

  return (
    <div className="app">
      <div className="app_left">
      {/*header */}
      {/*Titel + select input  */}
      <div className="app_header">
      <h1>Covid-19 tracker </h1>
        <FormControl className="app_dropdown">
          <Select variant="outlined" onChange={onCountryChange} value={country} >
          <MenuItem value="worldwide">worldwide</MenuItem>
            {
              countries.map(country => (
              <MenuItem value={country.value}>{country.name}</MenuItem>
              ))
            }
          </Select>
        </FormControl>
      </div>
      <div className="app_stats">
        <InfoBox 
          isRed
          active={casesType === "cases"}
          onClick={(e) => setCasesType('cases')}
          title="Coronavirus Cases" 
          cases={prettyPrintStat(countryInfo.todayCases)} 
          total={prettyPrintStat(countryInfo.cases)}
        />
        <InfoBox 
          active={casesType === "recovered"}
          onClick={(e) => setCasesType('recovered')}
          title="Recoverd" 
          cases={prettyPrintStat(countryInfo.todayRecovered)} 
          total={prettyPrintStat(countryInfo.recovered)}
        />
        <InfoBox 
          isRed
          active={casesType === "deaths"}
          onClick={(e) => setCasesType('deaths')}
          title="Deaths" 
          cases={prettyPrintStat(countryInfo.todayDeaths)} 
          total={prettyPrintStat(countryInfo.deaths)}
        />
      </div>

      <Map
          countries={mapCountries}
          casesType={casesType}
          center={mapCenter}
          zoom={mapZoom}
        />
      </div>
      <Card className="app_right">
        <CardContent>
          <h3> Live Cases by Country</h3>
            <Table countries={tableData}/>
          <h3 className="worldwide-cases"> Worldwide new {casesType}</h3>
            <LineGraph className="app_graph" casesType={casesType}/>
        </CardContent>
      </Card>
    </div>
  );
}

export default App;
